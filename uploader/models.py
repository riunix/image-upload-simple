# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.db import models
from django.forms import ModelForm
from django.conf import settings
from uuid import uuid4
from datetime import datetime
from PIL import Image

IMG_THUMB_SIZE = 200,200
IMG_TYPE_MAP = {
    ".jpg"  : "JPEG",
    ".jpeg" : "JPEG",
    ".gif"  : "GIF",
    ".png"  : "PNG"
}

def get_upload_dest(object,filename):
	fl_nm,ext = os.path.splitext(filename)
	new_name = "%s%s" % (uuid4().hex[:8],ext.lower())
	date_folder = datetime.now().strftime("%Y/%m/%d")
	return "photos/%s/%s" % (date_folder,new_name)

class Upload(models.Model):
    img    = models.ImageField(upload_to=get_upload_dest)  
    status = models.SmallIntegerField(default=0) #TODO: will be used for flag to tell if the image is ready to be viewed
    time   = models.DateTimeField(auto_now_add =True)

    def save(self,*args, **kwargs):
    	super(Upload,self).save(*args, **kwargs)
    	self.generate_thumb()

    def generate_thumb(self):
		#TODO: Send the process to Celery instead direct process(async), so the request will return to user faster. Need to update status field become one if thumb is generated
		fl_nm,ext = os.path.splitext(self.img.name)
		img_type  = IMG_TYPE_MAP.get(ext,False)
		if not img_type: return False

		new_name = "%s/%s_thumb%s" % (settings.MEDIA_ROOT,fl_nm,ext)
		img = Image.open(self.img)     	
		img.thumbnail(IMG_THUMB_SIZE)
		img.save(new_name,img_type)

class UploadForm(ModelForm):
    class Meta:
        model = Upload
        fields = ('img',)