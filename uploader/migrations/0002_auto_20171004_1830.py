# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-04 18:30
from __future__ import unicode_literals

from django.db import migrations, models
import uploader.models


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='upload',
            name='status',
            field=models.SmallIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='upload',
            name='img',
            field=models.ImageField(upload_to=uploader.models.get_upload_dest),
        ),
    ]
