# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from uploader.models import UploadForm,Upload


def home(request):
    if request.method=="POST":
        img = UploadForm(request.POST, request.FILES)       
        if img.is_valid():
            img.save()  
            return HttpResponseRedirect(reverse('upload_home'))
    else:
        img=UploadForm()
    
    #TODO: need to move data select become API, so can be called by ajax call and need to make pagination also to minimize the load
    images=Upload.objects.all() #TODO: after make is become async process for the thumb, then need to add filter status=1

    return render(request,'home.html',{'form':img,'images':images})