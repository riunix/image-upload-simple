from django import template
import os
register = template.Library()

@register.filter
def is_thumb(filename):
	fl_nm,ext = os.path.splitext(filename)
	return "%s_thumb%s" % (fl_nm,ext)
    